class Intakter{
  constructor(name, pages, id){
    this.name = name
    this.pages = pages
    this.id = id
  }
}
var Intakt = {
  people : [
    new Intakter("Hoóz Anna", [6,7,8], "ha"),
    new Intakter("Németh Dániel", [4,5], "nd"),
    new Intakter("Plesznivy Ákos", [], "pa"),
    new Intakter("Tihanyi Áron", [1,2,3], "ta")
  ],
  pages : [
    "home page",
    "amazeingenerativestyle",
    "cossin",
    "mondrianianquadtree",
    "rectpaint",
    "negysoros",
    "buttoff",
    "redsome",
    "rockmusic",
    "memories"
  ],
  current : -1,
  idleLimit : 369,
  idleTime : 0,

  instantSwitch : function(i){
    if(i != undefined){
      window.location = "../../pages/" + Intakt.pages[i] + "/index.html"
    }
    else{
      let next = (Intakt.current + 1) % Intakt.pages.length
      if(next == 0) next = 1
      window.location = "../../pages/" + Intakt.pages[next] + "/index.html"
    }
  },
  watchIdle : function(){
    Intakt.idleTime += 1
    if(Intakt.idleTime >= Intakt.idleLimit)
      Intakt.instantSwitch()
    // console.log(Intakt.idleTime)
  },
  resetIdle : function(){
    Intakt.idleTime = 0
  },
  keys(e){
    let i = e.keyCode - 48
    if(i >= 0 && i < Intakt.pages.length)
      Intakt.instantSwitch(i)
    else
      Intakt.resetIdle()

  },
  initIdle : function(){
    document.addEventListener("contextmenu", (e)=>e.preventDefault())
    document.addEventListener("mousemove", Intakt.resetIdle)
    document.addEventListener("keydown", Intakt.keys)
    document.addEventListener("mousedown", Intakt.resetIdle)
    window.setInterval(Intakt.watchIdle, 1000)
  },
  init : function(path, idler, timeout){
    Intakt.current = Intakt.pages.indexOf(path.substr(7).split("/")[0])
    if(idler) Intakt.initIdle()
    if(timeout) Intakt.idleLimit = timeout
  }
}
