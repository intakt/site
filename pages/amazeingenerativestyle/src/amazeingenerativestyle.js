var x = 0;
var y = 0;

var para = {
  chance : 0.5,
  spacing : 10,
  size : 0,
  alpha : 1,
  sweight : 1,
  color : "#ffffff",
  color2 : "#ffffff",
  x1 : 0,
  x2 : 0,
  y1 : 0,
  y2 : 0,
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
  background(0);
}

function setup(){
  createCanvas(windowWidth, windowHeight);
  background(0);
}

var gui = new dat.GUI()
gui.add(para, "chance", 0, 1).name('ratio').step(0.01).listen()
gui.add(para, "spacing", 0, 50).name('size').step(0.01).listen()
gui.add(para, "size", 0, 100).name('spacing').step(0.01).listen()
gui.add(para, "alpha", 0, 5).name('fade out').step(0.01).listen()
gui.add(para, "sweight", 0.5, 10).name('strokeweight').step(0.01).listen()
gui.addColor(para,"color").name('first color').listen()
gui.addColor(para,"color2").name('second color').listen()
gui.add(para, "x1", -50, 50).name('x backward').step(0.01).listen()
gui.add(para, "x2", -50, 50).name('x forward').step(0.01).listen()
gui.add(para, "y1", -50, 50).name('y backward').step(0.01).listen()
gui.add(para, "y2", -50, 50).name('y forward').step(0.01).listen()

function draw(){
  background(0,0,0,para.alpha)
  strokeWeight(para.sweight);
  if(random(1) < para.chance){
    stroke(para.color);
    line(
      x + para.x1,
      y + para.y1,
      x + para.spacing,
      y + para.spacing
    );
  }else
    line(
      x - para.x2,
      y + para.spacing - para.y2,
      x + para.spacing,
      y
    );
    stroke(para.color2);
      x = x + para.spacing + para.size;
    if(x > width){
      x = 0;
      y = y + para.spacing;
    }
    if(y > height){
      y = 0
    }
  }
