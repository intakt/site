precision mediump float;
uniform vec4 uMaterialColor;
uniform vec3 screenColor;
uniform sampler2D uSampler;
uniform bool isTexture;
uniform bool uUseLighting;
varying vec3 vLightWeighting;
varying highp vec2 vVertTexCoord;

vec3 rgb2hsv(vec3 rgb)
{
    float Cmax = max(rgb.r, max(rgb.g, rgb.b));
    float Cmin = min(rgb.r, min(rgb.g, rgb.b));
    float delta = Cmax - Cmin;

    vec3 hsv = vec3(0., 0., Cmax);
    
    if (Cmax > Cmin)
    {
        hsv.y = delta / Cmax;

        if (rgb.r == Cmax)
            hsv.x = (rgb.g - rgb.b) / delta;
        else
        {
            if (rgb.g == Cmax)
                hsv.x = 2. + (rgb.b - rgb.r) / delta;
            else
                hsv.x = 4. + (rgb.r - rgb.g) / delta;
        }
        hsv.x = fract(hsv.x / 6.);
    }
    return hsv;
}

float chromaKey(vec3 color)
{
    // vec3 backgroundColor = vec3(0.18, 0.576, 0.129);

    vec3 weights = vec3(5., 1., 2.);

    vec3 hsv = rgb2hsv(color);
    vec3 target = rgb2hsv(screenColor);
    float dist = length(weights * (target - hsv));
    return 1. - clamp(3. * dist - 1.5, 0., 1.);
}


void main() {
    vec4 c = texture2D(uSampler, vVertTexCoord);
    if(chromaKey(c.rgb) > 0.4)
    gl_FragColor = 
        isTexture 
        ? vec4(0,0,0,0) 
        : uMaterialColor;
    else
        gl_FragColor = 
        isTexture 
        ? texture2D(uSampler, vVertTexCoord) 
        : uMaterialColor;
    
      if (uUseLighting)
          gl_FragColor.rgb *= vLightWeighting;
}
