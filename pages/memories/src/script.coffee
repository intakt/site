imgs = []
variations = 6
counter = 0

time = 0
rate = 42

lasttime = 0

w = innerWidth/2
h = innerHeight/2
l = if w > h then w else h

pixelated = null


birandom = () ->
  -1 + Math.random() * 2

clamp = (v, a, b) ->
  if v < a then a else if v > b then b else v

generateimages = () ->
  imgs = []
  variations = Math.floor(6 + Math.random() * 4)
  for i in [0..variations - 1]
    im = createImage(l,l, RGB)
    im.loadPixels()
    for x in [0..l-1]
      for y in [0..l-1]
        # c = if Math.random() > 0.5 then 0 else 255
        im.set x, y, [(if Math.random() > 0.5 then 0 else 255),(if Math.random() > 0.4 then 0 else 255),(if Math.random() > 0.5 then 0 else 164),255]
    # for p in [0..im.pixels.length - 1]
    #   im.pixels[p] = color(c, c, c, 255)
    im.updatePixels()
    console.log im
    imgs.push im
preload = () ->
  pixelated = loadShader("shader/default.vert", "shader/chroma.frag")
setup = () ->
  createCanvas(innerWidth, innerHeight, WEBGL)

  generateimages()

sprayimage = (c, x, y, spread) ->
  parts = 100
  console.log x
  console.log y
  # imgs[counter].loadPixels()
  for i in [0..parts - 1]
    a = Math.random() * Math.PI * 2
    r = Math.random() * spread
    xs = x + Math.sin(a) * r
    ys = y + Math.cos(a) * r
    fill c
    rect(xs, ys, 2,2)
    # imgs[counter].set clamp(floor(xs), 0, w), clamp(floor(ys), 0, h), [c, c, c, 255]   
  # imgs[counter].updatePixels()


mousedown = (e) ->
  console.log e.button 
  sprayimage (if e.button is 2 then 0 else 255) , mouseX / 2, mouseY / 2, 20

draw = () ->
  clear()
  delta = (millis() - lasttime) * 0.001
  nt = time + rate * delta
  time = nt - floor(nt)
  lasttime = millis()
  if floor(nt) > 0
    counter = (counter + 1) % imgs.length
    
  # fill 255
  # rect 0, 0, 200, 200
  zoom = height / 2 - 80
  camera(0, 0, -l / 3 + sin(frameCount * 0.004) * 60, 0, 1, 0, 0, 1, 0);
  tint(255, 255)
  rotateZ(frameCount * 0.001)
  pixelated.setUniform("screenColor", [0, 0, 100])

  shader(pixelated)
  texture(imgs[(counter + 1) % imgs.length])
  beginShape()
  vertex(-l * 0.5,-l * 0.5,0,0,0)
  vertex(l * 0.5,-l * 0.5,0,1,0)
  vertex(l * 0.5,l * 0.5,0,1,1)
  vertex(-l * 0.5,l * 0.5,0,0,1)
  endShape(CLOSE)
  # image(imgs[(counter + 1) % imgs.length],0,0)
  tint(255, (1 - time) * 255)
  texture(imgs[counter])
  shader(pixelated)
  beginShape()
  vertex(-l * 0.5,-l * 0.5,0,0,0)
  vertex(l * 0.5,-l * 0.5,0,1,0)
  vertex(l * 0.5,l * 0.5,0,1,1)
  vertex(-l * 0.5,l * 0.5,0,0,1)
  endShape(CLOSE)
  # image(imgs[counter],0,0)
  # sprayimage(0, mouseX, mouseY, 60)

$("html").on("contextmenu", (e) -> e.preventDefault())
$("html").on("mousedown", (e) -> generateimages())

