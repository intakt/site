// Generated by CoffeeScript 2.2.4
var birandom, clamp, counter, draw, generateimages, h, imgs, l, lasttime, mousedown, pixelated, preload, rate, setup, sprayimage, time, variations, w;

imgs = [];

variations = 6;

counter = 0;

time = 0;

rate = 42;

lasttime = 0;

w = innerWidth / 2;

h = innerHeight / 2;

l = w > h ? w : h;

pixelated = null;

birandom = function() {
  return -1 + Math.random() * 2;
};

clamp = function(v, a, b) {
  if (v < a) {
    return a;
  } else if (v > b) {
    return b;
  } else {
    return v;
  }
};

generateimages = function() {
  var i, im, j, k, m, ref, ref1, ref2, results, x, y;
  imgs = [];
  variations = Math.floor(6 + Math.random() * 4);
  results = [];
  for (i = j = 0, ref = variations - 1; (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
    im = createImage(l, l, RGB);
    im.loadPixels();
    for (x = k = 0, ref1 = l - 1; (0 <= ref1 ? k <= ref1 : k >= ref1); x = 0 <= ref1 ? ++k : --k) {
      for (y = m = 0, ref2 = l - 1; (0 <= ref2 ? m <= ref2 : m >= ref2); y = 0 <= ref2 ? ++m : --m) {
        // c = if Math.random() > 0.5 then 0 else 255
        im.set(x, y, [(Math.random() > 0.5 ? 0 : 255), (Math.random() > 0.4 ? 0 : 255), (Math.random() > 0.5 ? 0 : 164), 255]);
      }
    }
    // for p in [0..im.pixels.length - 1]
    //   im.pixels[p] = color(c, c, c, 255)
    im.updatePixels();
    console.log(im);
    results.push(imgs.push(im));
  }
  return results;
};

preload = function() {
  return pixelated = loadShader("shader/default.vert", "shader/chroma.frag");
};

setup = function() {
  createCanvas(innerWidth, innerHeight, WEBGL);
  return generateimages();
};

sprayimage = function(c, x, y, spread) {
  var a, i, j, parts, r, ref, results, xs, ys;
  parts = 100;
  console.log(x);
  console.log(y);
// imgs[counter].loadPixels()
  results = [];
  for (i = j = 0, ref = parts - 1; (0 <= ref ? j <= ref : j >= ref); i = 0 <= ref ? ++j : --j) {
    a = Math.random() * Math.PI * 2;
    r = Math.random() * spread;
    xs = x + Math.sin(a) * r;
    ys = y + Math.cos(a) * r;
    fill(c);
    results.push(rect(xs, ys, 2, 2));
  }
  return results;
};

// imgs[counter].set clamp(floor(xs), 0, w), clamp(floor(ys), 0, h), [c, c, c, 255]   
// imgs[counter].updatePixels()
mousedown = function(e) {
  console.log(e.button);
  return sprayimage((e.button === 2 ? 0 : 255), mouseX / 2, mouseY / 2, 20);
};

draw = function() {
  var delta, nt, zoom;
  clear();
  delta = (millis() - lasttime) * 0.001;
  nt = time + rate * delta;
  time = nt - floor(nt);
  lasttime = millis();
  if (floor(nt) > 0) {
    counter = (counter + 1) % imgs.length;
  }
  
  // fill 255
  // rect 0, 0, 200, 200
  zoom = height / 2 - 80;
  camera(0, 0, -l / 3 + sin(frameCount * 0.004) * 60, 0, 1, 0, 0, 1, 0);
  tint(255, 255);
  rotateZ(frameCount * 0.001);
  pixelated.setUniform("screenColor", [0, 0, 100]);
  shader(pixelated);
  texture(imgs[(counter + 1) % imgs.length]);
  beginShape();
  vertex(-l * 0.5, -l * 0.5, 0, 0, 0);
  vertex(l * 0.5, -l * 0.5, 0, 1, 0);
  vertex(l * 0.5, l * 0.5, 0, 1, 1);
  vertex(-l * 0.5, l * 0.5, 0, 0, 1);
  endShape(CLOSE);
  // image(imgs[(counter + 1) % imgs.length],0,0)
  tint(255, (1 - time) * 255);
  texture(imgs[counter]);
  shader(pixelated);
  beginShape();
  vertex(-l * 0.5, -l * 0.5, 0, 0, 0);
  vertex(l * 0.5, -l * 0.5, 0, 1, 0);
  vertex(l * 0.5, l * 0.5, 0, 1, 1);
  vertex(-l * 0.5, l * 0.5, 0, 0, 1);
  return endShape(CLOSE);
};

// image(imgs[counter],0,0)
// sprayimage(0, mouseX, mouseY, 60)
$("html").on("contextmenu", function(e) {
  return e.preventDefault();
});

$("html").on("mousedown", function(e) {
  return generateimages();
});

//# sourceMappingURL=script.js.map
