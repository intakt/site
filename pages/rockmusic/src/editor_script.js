const img_width = 512
var vertices = []
var rocks = []

function distance(a, b){
  var _a = a.x - b.x
  var _b = a.y - b.y
  return Math.sqrt( _a*_a + _b*_b )
}

function parse_rock(rock){
  var r = $.extend(true, {}, rock)
  var center = r.mesh.vertices.reduce(
    function(a, v){
      return {
        x : a.x + v.x/r.mesh.vertices.length,
        y : a.y + v.y/r.mesh.vertices.length
      }
    }, {x:0, y:0}
  )
  var uvs = r.mesh.vertices.map(function(v){return { x : v.x / img_width, y : v.y / img_width }})
  r.mesh.uvs = uvs
  r.radius = r.mesh.vertices.reduce((a, v) => distance(v, center) > a ? distance(v, center) : a, 0)
  r.mesh.vertices = r.mesh.vertices.map(function(v){return {x:v.x - center.x, y:v.y - center.y}})
  r.absolute_vertices = rock.mesh.vertices
  return r
}

function new_rock(verts, name, sound){
  return {
    mesh:{vertices:verts},
    name : ($("#name").val() == "" ? "rock" + rocks.length : $("#name").val()),
    sound : ($("#sound").val() == "" ? "./media/rock_sample.mp3" : $("#sound").val())
  }
}

function refresh_rocks_menu(rs){
  $("#rocks").html( rs.map(r=>$("<button>", {html:r.name})) )
}

function create_new_rock(){
  if(vertices.length > 2){
    var r = new_rock(vertices)
    rocks.push(r)
    vertices = []
    refresh_rocks_menu(rocks)
  }else{
    $("#output").html("a rock must have at least 3 corners!")
  }
}

function preload(){
  img = loadImage("./images/rock.png")
}
function setup(){
  createCanvas(512, 512).parent("p5_container")
}

function draw_verts(verts, color){
  verts.forEach(
    function(v, i){
      var next = verts[(i+1) % verts.length]
      stroke(color)
      fill("#fff")
      ellipse(v.x,v.y,10,10)
      textSize(16)
      fill("#000")
      text(i,v.x,v.y)
      line(v.x,v.y,next.x,next.y)
    }
  )
}

function draw(){
  image(img,0,0,width,height)
  draw_verts(vertices, "#fd2")
  rocks.forEach((r) => draw_verts(r.mesh.vertices, "#0f6"))
}

$("#p5_container").click(
  function(e){
    vertices.push({x : e.offsetX, y : e.offsetY})
  }
)
$(window).keyup(
  function(e){
    if(e.key == "z") vertices.pop()
    else if(e.key == " " || e.key == "enter") create_new_rock()
  }
)

$("#new_rock").click((e)=>create_new_rock())

$("#print").click((e)=>$("#output").html("var ROCKS = `" + JSON.stringify( rocks.map((r)=>parse_rock(r)) ) + "`"))

$("#load").click(
  function(e){
    rocks = JSON.parse($("#input").val()).map(
      function(r){
        return {
          name : r.name,
          mesh : {vertices : r.absolute_vertices},
          sound : r.sound
        }
      }
    )
    refresh_rocks_menu(rocks)
    console.log(rocks)
  }
)
