let info = `intakt = {
  
  info : "egy önszerveződő műhely-kísérlet aminek 
         célja a résztvevők segítése a programozással
         való ismerkedésükben",

         // a self-organizing workshop made to
         // help participants learn programming

  people : [
    "Hoóz Anna",
    "Németh Dániel",
    "Plesznivy Ákos", // guide
    "Tihanyi Áron"
  ],

  pages : [
    "home page", // 0
    "amazeingenerativestyle", // 1
    "cossin", // 2
    "mondrianianquadtree", // 3
    "rectpaint", // 4
    "negysoros" // 5
    "buttoff", // 6
    "redsome", // 7
    "rockmusic", // 8
  ],

  hint : "az oldalak között a hozzájuk kapcsolódó
         sorszámokkal tudsz navigálni,
         az egér sem felesleges,
         ha szeretnéd elmenteni amit látsz 
         nyomd meg az szóköz billentyűt",

         // navigate the pages with the number keys
         // use the mouse
         // save screenshot with SPACE
         
  site : "intakt.gitlab.io/site",
  // az elmentett képek, itt lesznek megtekinthetőek
  // saved images will be uploaded to this link

}`



$("a").mouseenter(function (e){
  $(".highlighted").removeClass("highlighted")
  let intakter = Intakt.people.find( p => p.pages.indexOf(Intakt.pages.indexOf(e.currentTarget.innerHTML)) != -1 )
  if(intakter != undefined) 
    $("#"+intakter.id).addClass("highlighted")
}) 
$("a").mouseleave(function (e){
  $(".highlighted").removeClass("highlighted")
})
