var alanyok = ["folyó", "vaskorlát", "éjszaka", "éj", "sötétség", "szél",
  "fagy", "part", "út", "fa", "kavics", "űr", "verssor", "sínpár",
  "séta", "hold", "semmi", "hiány", "realitás", "füst", "kép", "pad",
  "levegő", "víztükör", "hajnal", "vakolat", "pupilla", "bokor", "hiány", "szédület", "bolyongás", "szövet", "fáradtság"
]
var hatarozok = ["fáradtan", "botladozva", "észrevétlen", "hangtalanul", "némán", "csöndesen", "céltalanul", "bizonytalanul", "vékonyan", "lassan", "félve", "sötéten", "tompán", "tántorogva", "üresen", "lemondóan", "rezignáltan", ]
var allitmanyok = ["szűkül", "lehull", "néz", "sétál", "csöpög", "járkál", "járkál",
  "hazajut", "dúdol", "nő", "világít", "hull", "elázik", "kopog", "ázik",
  "csatangol", "zúg", "kérdez", "meghal", "marad", "letörik", "tágul",
  "közelít", "kiszárad", "tántorog", "bolyong", "elhagy", "súlyosodik",
  "körbeér", "megbillen", "rohan", "megnyílik", "eltapos", "zörög", "esteledik"
]
var jelzok = ["sötét", "hideg", "mély", "fehér", "fénylő", "bírhatatlan", "üres", "biztos", "fáradt", "végtelen", "vékony", "száraz", "vetített", "fulladt", "bomló", "lassú", "törékeny", "tompított", "száradt",
  "tátongó", "formátlan", "szédült", "hűs", "örökös", "tompa", "tétova", "szűk", "tág", "omló", "hirtelenszürke", "halott", "zúgó", "tornyosuló", "karcos", "szaggatott", "alaktalan", "elhagyatott", "idegen", "kihalt"
]
var szeles = 9*51+15
var sormag = 25
var magassag = innerHeight/2-2*sormag


var randomFrom = function(bejovo_lista) {
  var randomIndex = Math.floor(Math.random() * bejovo_lista.length)
  return bejovo_lista[randomIndex]
}
var limiter = new Tone.Limiter(0).toMaster();
var pingPong = new Tone.PingPongDelay("0.2", 0.3).connect(limiter);
var filter = new Tone.Filter(220, "lowpass", -24).connect(pingPong).set(
  {"Q" : 0})
var szinti = new Tone.PolySynth(4, Tone.FMSynth).connect(filter);
  szinti.set({
    "envelope" : {attack:0.1, decay: 0.1, sustain: 0.8, release: 0.3},
    "oscillator" : {type: "sine"}
  })

Tone.Buffer.on("load", function() {
  player.start()
})
// for (var i = 0; i < szinti.voices.length; i++) {
//   var t = randomFrom(["sine", "triangle"])
//   szinti.voices[i].oscillator.type = t
// }

var maganhangzovalKezdodik = function(input_szoveg) {
  var maganhangzok = "aáeéiíoóöőuúüű"
  var elsobetu = input_szoveg[0]
  var index = maganhangzok.indexOf(elsobetu)
  return index > -1
}
var randomMondat = function() {
  var random_jelzo = randomFrom(jelzok)
  var random_alany = randomFrom(alanyok)
  var random_allitmany = randomFrom(allitmanyok)
  var random_hatarozo = randomFrom(hatarozok)
  var nevelo = ""
  if (maganhangzovalKezdodik(random_jelzo)) {
    nevelo = "az"
  } else {
    nevelo = "a"
  }
  return nevelo + " " + random_jelzo + " " + random_alany + " " + random_hatarozo + " " + random_allitmany
}
var randomPos = function() {
  var pos = {
    left: (Math.random() * (window.innerWidth - szeles)) + "px",
    top: (Math.random() * (window.innerHeight - sormag)) + "px"
  }
  return pos
}
var hsl = function(h, s, l) {
  return "hsl(" + h + "," + s + "%," + l + "%)"
}
var randomColor = function() {
  return hsl(190 + Math.random() * 50, 50, 15 + Math.random() * 50)
}
var randomHang = function() {
  var hangok = "CDEFGBA"
  return randomFrom(hangok)
}
var postMondat = function() {
  var pos = randomPos()
  var elemObject = {
    html: randomMondat(),
    class: randomHang(),
    css: {
      position: "absolute",
      width: szeles + "px",
      left: pos.left,
      top: pos.top,
      "text-align": "left",
      "font-size": 15 + "px",
      padding: 6 + "px",
      color: "#fff",
      "background-color": randomColor(),
    },

    click: function() {
      if (!$(this).hasClass("verssor")) {
        $(this).addClass("verssor")
        $(this).click(function(e){
          $(this).html(randomMondat()).css({
            "background-color" : randomColor()
          })
          szinti.triggerAttackRelease(randomHang() + 4, "0.7")
        })
        var h = $(this).attr("class")[0] + 4
        szinti.context.resume()
        szinti.triggerAttackRelease(h, "0.7")
        $(this).animate({
          left: (window.innerWidth * 0.5 - (szeles/2)) + "px",
          top: magassag + "px"
        }, 1000)
        magassag += $(this).outerHeight()
        console.log(magassag)
      }
    }
  }
  var elemJ = $("<div>", elemObject)
  $("body").append(elemJ)
}
var loopy = function() {
  for (var i = 0; i < 4; i++) {
    postMondat()
  }
}

loopy()
