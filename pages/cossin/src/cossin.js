var frameCount = 0;

var para = {
  x1 : 1,
  y1 : 1,
  a1 : 0,
  b1 : 0,
  x2 : 1,
  y2 : 1,
  a2 : 0,
  b2 : 0,
  csin : 10,
  ccos : 10,
  color : "#666666",
  alpha : 2.5,
}

function windowResized(){
  resizeCanvas(windowWidth, windowHeight);
}

function setup(){
  createCanvas(windowWidth, windowHeight);
}

function clearCanvas(value){
  createCanvas(windowWidth, windowHeight);
}

var gui = new dat.GUI();
gui.add(para, 'x1', 0, 10).step(0.1).name('x<sub>1</sub>').listen().onChange(clearCanvas);
gui.add(para, 'y1', 0, 10).step(0.1).name('y<sub>1</sub>').listen().onChange(clearCanvas);
gui.add(para, 'a1', 0, 10).step(0.1).name('a<sub>1</sub>').listen().onChange(clearCanvas);
gui.add(para, 'b1', 0, 10).step(0.1).name('b<sub>1</sub>').listen().onChange(clearCanvas);
gui.add(para, 'x2', 0, 10).step(0.1).name('x<sub>2</sub>').listen().onChange(clearCanvas);
gui.add(para, 'y2', 0, 10).step(0.1).name('y<sub>2</sub>').listen().onChange(clearCanvas);
gui.add(para, 'a2', 0, 10).step(0.1).name('a<sub>2</sub>').listen().onChange(clearCanvas);
gui.add(para, 'b2', 0, 10).step(0.1).name('b<sub>2</sub>').listen().onChange(clearCanvas);
gui.add(para, 'csin', 0, 10).step(0.1).name('sin').listen().onChange(clearCanvas);
gui.add(para, 'ccos', 0, 10).step(0.1).name('cos').listen().onChange(clearCanvas);
gui.addColor(para,'color').name('color').listen();
gui.add(para, 'alpha', 0, 5).name('fade out').step(0.01).listen();

function draw(){
  background(0, 0, 0, para.alpha);
  stroke(para.color);
  fill(para.color);
    frameCount++;
    var x = sin(frameCount * para.x1 + para.a1) * height / 2.1;
    var y = cos(frameCount * para.y1 + para.b1) * height / 2.1;

    var xOld = sin((frameCount - para.csin) * para.x2 + para.a2) * height / 2.1;
    var yOld = cos((frameCount - para.ccos) * para.y2 + para.b2) * height / 2.1;

    translate(width / 2, height / 2);
    ellipse(x, y, 1, 1);
    ellipse(xOld, yOld, 1, 1);
    line(x, y, xOld, yOld);
}
