class Point {
  constructor(x, y) {
    this.x = x;
    this.y = y;
  }
}

class Rectangle {
  constructor(x, y, w, h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }
  contains(point){
    return(point.x >= this.x - this.w &&
    point.x <= this.x + this.w &&
    point.y >= this.y - this.h &&
    point.y <= this.y + this.h);
  }
}

class QuadTree {
  constructor(boundary, n){
    this.boundary = boundary;
    this.capacity = n;
    this.points = [];
    this.divided = false;
  }

  subdivide(){
    var x = this.boundary.x;
    var y = this.boundary.y;
    var w = this.boundary.w;
    var h = this.boundary.h;

    var ne = new Rectangle(x + w / 2, y - h / 2, w / 2, h / 2);
    this.northeast = new QuadTree(ne, this.capacity);
    var nw = new Rectangle(x - w / 2, y - h / 2, w / 2, h / 2);
    this.northwest = new QuadTree(nw, this.capacity);
    var se = new Rectangle(x + w / 2, y + h / 2, w / 2, h / 2);
    this.southeast = new QuadTree(se, this.capacity);
    var sw = new Rectangle(x - w / 2, y + h / 2, w / 2, h / 2);
    this.southwest = new QuadTree(sw, this.capacity);
    this.divided = true;
  }

  insert(point){
    if(!this.boundary.contains(point)){
      return false;
    }

    if(this.points.length < this.capacity){
      this.points.push(point);
    }else{
      if(!this.divided){
        this.subdivide();
        return true;
      }
      if(this.northeast.insert(point)){
        return true;
      }
      if(this.northwest.insert(point)){
        return true;
      }
      if(this.southeast.insert(point)){
        return true;
      }
      if(this.southwest.insert(point)){
        return true;
      }
    }
  }

  show(){
    var colors = [para.color1, para.color2, para.color3, para.color4, para.color5];
    var color = colors[Math.floor(Math.random() * colors.length)];
    stroke(10);
    strokeWeight(windowHeight/120);
    fill(color);
    rectMode(CENTER);
    rect(this.boundary.x, this.boundary.y, this.boundary.w * 2, this.boundary.h * 2);
    if(this.divided){
      this.northeast.show();
      this.northwest.show();
      this.southeast.show();
      this.southwest.show();
    }
  }
}
