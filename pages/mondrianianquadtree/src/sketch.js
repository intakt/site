var qtree;
var para = {
  color1 : "#f9f9f9",
  color2 : "#fff001",
  color3 : "#ff0101",
  color4 : "#0101fd",
  color5 : "#101010",
  fr : 6,
}

function windowResized(){
  createCanvas(windowHeight, windowHeight);
  var boundary = new Rectangle(0,windowHeight,windowHeight,windowHeight);
  qtree = new QuadTree(boundary, 1);
  console.log(qtree);
}

function setup(){
  createCanvas(windowHeight, windowHeight);
  frameRate(6);
  var boundary = new Rectangle(0,windowHeight,windowHeight,windowHeight);
  qtree = new QuadTree(boundary, 1);
  qtree.insert(new Point(windowHeight * 0.5, windowHeight * 0.5));
  qtree.insert(new Point(windowHeight * 0.5, windowHeight * 0.5));
}

var gui = new dat.GUI()
gui.addColor(para,"color1").name('color #1').listen()
gui.addColor(para,"color2").name('color #2').listen()
gui.addColor(para,"color3").name('color #3').listen()
gui.addColor(para,"color4").name('color #4').listen()
gui.addColor(para,"color5").name('color #5').listen()
gui.add(para, "fr", 1, 60).name('frame rate').step(1).listen()

function mousePress(e){
  var m = new Point(e.offsetX, e.offsetY);
  qtree.insert(m);
  qtree.insert(m);
}

function draw(){
  frameRate(para.fr);
  background(255);
  background(255);
  qtree.show();
}

document.addEventListener("click", mousePress)
